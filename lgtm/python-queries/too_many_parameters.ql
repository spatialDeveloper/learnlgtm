/**
 * @name A function has too many parameters
 * @description A function with more than 7 parameters is likely to be too complex.
 * @kind problem
 * @tags maintainability
 * @problem.severity error
 * @sub-severity low
 * @precision high
 * @id py/function-too-many-params
 */

import python

from Function fcn
where count(fcn.getAnArg()) > 7
select fcn, "Function has too many parameters"